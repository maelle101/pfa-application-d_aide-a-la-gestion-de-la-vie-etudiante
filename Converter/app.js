const CSVToJSON = require("csvtojson");
const FileSystem = require("fs");

var source = '../Data/dataSyllabus.csv';
var destination = '../Data/dataSyllabus.json';

// Convert a CSV file to a JSON file
// If there is problems with UTF8 characters, use this on your csv files : iconv CSV_File -f latin1 -t utf-8 -o CSV_File
// If there is spaces in your CSV file's fields, use this : sed -i '1 s/ /_/g' CSV_File

// Convert your source file to a JSON array
CSVToJSON({delimiter:";"/*, ignoreEmpty:true*/})
    .fromFile(source)
    .then(src => {

        // source is a JSON array

        // Write JSON array to a file
        FileSystem.writeFile(destination, JSON.stringify(src, null, 4), (err) => {
            if (err) {
                throw err;
            }
            console.log("Conversion successfully done !");
        });

        // log the JSON array :
        // console.log(source);

    }).catch(err => {
        // log error if any
        console.log(err);
    });
