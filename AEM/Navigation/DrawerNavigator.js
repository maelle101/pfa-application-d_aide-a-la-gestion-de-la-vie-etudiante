import { createDrawerNavigator} from '@react-navigation/drawer';
import React from 'react';

import Home from '../Components/Home';
import Settings from '../Components/Settings';
import Map from '../Components/Plan/Map'
import Syllabus from '../Components/Syllabus/Syllabus';
import MessagingService from '../Components/MesagingService';
import Voluntary from '../Components/Voluntary';
import Mailbox from '../Components/Mailbox';
import Edt from '../Components/EDT/Edt';



const Drawer = createDrawerNavigator();

export default function DrawerNavigator({params,id}) {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name="Accueil"
        component={Home}
	  />
	  <Drawer.Screen
	    name="Emploi du temps"
	    component={Edt}
      initialParams={{ params: params, id:id }}
	  />
      <Drawer.Screen
        name="Mails"
        component={Mailbox}
      />
      <Drawer.Screen
        name="Carte"
        component={Map}
      />
      <Drawer.Screen
        name="Messagerie"
        component={MessagingService}
      />
      <Drawer.Screen
        name="Syllabus"
        component={Syllabus}
      />
      <Drawer.Screen
        name="Associatif"
        component={Voluntary}
      />
      <Drawer.Screen
        name="Paramètres"
        component={Settings}
      />
    </Drawer.Navigator>
  );
}
