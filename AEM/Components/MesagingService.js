import React from 'react'
import { View, StyleSheet } from 'react-native'
import Banner from './Banner'
import WIP from './WIP'

class MessagingService extends React.Component {

  render() {
    return (
      <View style={styles.main_container}>
        <Banner navigation= {this.props.navigation} />
        <WIP
          description="Ici sera bientôt disponible la messagerie."
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'flex-start'
  }
})

export default MessagingService
