import React from 'react'
import { View, StyleSheet } from 'react-native'
import Banner from './Banner'
import WIP from './WIP'

class Voluntary extends React.Component {

  render() {
    return (
      <View style={styles.main_container}>
        <Banner navigation= {this.props.navigation} />
        <WIP
          description="Ici seront bientôt disponible des informations sur la vie associative."
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'flex-start'
  }
})

export default Voluntary
