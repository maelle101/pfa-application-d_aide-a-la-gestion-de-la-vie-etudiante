import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet, FlatList } from 'react-native'

import Semester from './Semester'
import SimpleItem from './SimpleItem'
import { setShouldShow, extractDataMatchingFather } from './syllabus_utilities'

class ChoiceSemester extends React.Component {

  constructor(props) {
    super(props)
    this.state = { shouldShow: false }
    this.semestersOfChoiceSemester = []
    extractDataMatchingFather(this.props.allSemesters, this.props.data.EP, this.semestersOfChoiceSemester)
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code=""
            responsible={this.props.data.Responsables}
            styles={styles}
          />
        </TouchableOpacity>
        {this.state.shouldShow ? (
          <View>
            <FlatList
              style={styles.list}
              data={this.semestersOfChoiceSemester}
              keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
              renderItem={({ item }) => (
                <Semester
                  data={item}
                  allCourses={this.props.allCourses}
                  allUE={this.props.allUE}
                  choiceUE={this.props.choiceUE}
                  choiceCourses={this.props.choiceCourses}
                />
              )}
            >
            </FlatList>
          </View>
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#cdd491',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
})
export default ChoiceSemester
