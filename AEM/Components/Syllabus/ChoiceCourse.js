import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList, Text } from 'react-native'

import Course from './Course'
import SimpleItem from './SimpleItem'
import { setShouldShow, extractSingleEntry } from './syllabus_utilities'

class ChoiseCourse extends React.Component {

  constructor(props) {
    super(props)
    this.state = { shouldShow: false }
    this.coursesOfChoiceCourse = []
    extractSingleEntry(this.props.allCourses, this.props.data.EP, this.coursesOfChoiceCourse)
  }



  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code={this.props.data.EP}
            responsible={this.props.data.Responsables}
            styles={styles}
          />
        </TouchableOpacity>
        <View>
          {this.state.shouldShow ? (
            <FlatList
              style={styles.list}
              data={this.coursesOfChoiceCourse}
              keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
              renderItem={({ item }) => (
                <Course
                  data={item}
                />
              )}
            >
            </FlatList>
          ) : null}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#dadfac',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontWeight: 'bold'
},
code: {
    fontStyle: 'italic'
},
ects: {
  fontStyle: 'italic'
}
})

export default ChoiseCourse
