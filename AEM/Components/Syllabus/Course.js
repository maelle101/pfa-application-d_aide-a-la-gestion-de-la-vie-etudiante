import React from 'react'
import { View, Text, StyleSheet, FlatList } from 'react-native'

// Component that displays a course entry
class Course extends React.Component {

  constructor(props) {
    super(props)
    // Stores list of teachers
    this.teachers = []
    this._parseTeacher(this.props.data.Intervenants)
  }

  // Method that parses 'Intervenants'-field string into a list of theacher 
  // and usernames
  _parseTeacher(string) {
    if (string != undefined) {
      const deleteNoise = string.replace(/:/g, "  ");
      this.teachers = deleteNoise.split(",")
    }
  }

  render() {
    return (
      <View style={styles.item}>

        {/* Displays header container with title and code of the course*/}
        <View style={styles.header_container}>
          <Text style={styles.title}>{this.props.data.Intitulé}</Text>
          <Text style={styles.code}>Code {this.props.data.EP}</Text>
        </View>

        {/* Displays how much time is spent for this course */}
        <View>
          <Text style={styles.subtitle}>Volume horaire :</Text>
          <Text>Cours magistraux : {this.props.data.Volume_CM}h</Text>
          <Text>Cours intégrés : {this.props.data.Volume_CI}h</Text>
          <Text>Travaux dirigés : {this.props.data.Volume_TD}h</Text>
          <Text>Travaux dirigés sur machine : {this.props.data.Volume_TDM}h</Text>
          <Text>Travaux pratiques : {this.props.data.Volume_TP}h</Text>
          <Text>Travaux pratique de terrain : {this.props.data.Volume_TPT}h</Text>
          <Text>Travaux individuels : {this.props.data.Volume_TI}h</Text>
          <Text>Projets : {this.props.data.Volume_PRJ}h</Text>
        </View>

        {/* Displays coefficient */}
        {this.props.data.Coefficient != "" ? (
        <View>
          <Text style={styles.subtitle}>Coefficient :</Text>
          <Text>{this.props.data.Coefficient}</Text>
        </View>
        ) : null}

        {/* Displays terms of notation*/}
        {this.props.data.Mcc != "" ? (
          <View>
            <Text style={styles.subtitle}>Modalités d'évaluation :</Text>
            <Text>{this.props.data.Mcc}</Text>
          </View>
        ) : null}

        {/* Displays responsibles (if they exist)*/}
        {this.props.data.Responsables != "" ? (
          <View style={styles.responsible_container}>
            <Text style={styles.subtitle}>Responsable(s):</Text>
            <Text>{this.props.data.Responsables}</Text>
          </View>
        ) : null}

        {/* Displays teachers (if they exists) */}
        {this.props.data.Intervenants != "" ? (
          <View style={styles.teacher_container}>
            <Text style={styles.subtitle}>Intervenants :</Text>
            <FlatList
              data={this.teachers}
              keyExtractor={item => item}
              renderItem={({ item }) => (
                <Text>{item}</Text>
              )}
            />
          </View>
        ) : null}

        {/* Displays language of the course */}
        <View>
          <Text style={styles.subtitle}>Langue du module</Text>
          <Text>Langue du cours : {this.props.data.Langue_cours}</Text>
          <Text>Langue des supports : {this.props.data.Langue_support}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
  },
  responsible_container: {
    flex: 1,
  },
  item: {
    flex: 1,
    backgroundColor: '#e7e9c7',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    justifyContent: 'space-between'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15
  },
  subtitle: {
    fontWeight: 'bold'
  },
  code: {
    fontStyle: 'italic'
  }
})

export default Course
