import React from 'react'
import { View, StyleSheet, FlatList, ScrollView } from 'react-native'

// Import Banner component which include the button toggling the drawer menu
import Banner from '../Banner'
// Import first component responsible of the recursive-layout architecture 
import Department from './Department'
// Import explanations about nomenclature of evaluation terms
import Nomenclature from './Nomenclature'
// Import utilities 
import { extractDataMatchingType } from './syllabus_utilities'



class Syllabus extends React.Component {

  // Constructor which extracts and sorts data according to the type of entry
  constructor(props) {
    super(props)
    // Import the Syllabus' data to use
    this.syllabus_data = require('../../Data/dataSyllabus.json')
    // Declare variable where sorted-data are stored
    this.department = []
    this.year = []
    this.semester = []
    this.choiceSemesters = []
    this.UE = []
    this.choiceUE = []
    this.courses = []
    this.choiceCourses = []
    extractDataMatchingType(this.syllabus_data, "DIPLOME", this.department);
    extractDataMatchingType(this.syllabus_data, "AN", this.year);
    extractDataMatchingType(this.syllabus_data, "SEMESTRE", this.semester);
    extractDataMatchingType(this.syllabus_data, "SEMESTRE_CHOIX", this.choiceSemesters);
    extractDataMatchingType(this.syllabus_data, "UE", this.UE);
    extractDataMatchingType(this.syllabus_data, "UE_CHOIX", this.choiceUE);
    extractDataMatchingType(this.syllabus_data, "MODULE", this.courses);
    extractDataMatchingType(this.syllabus_data, "MODULE_CHOIX", this.choiceCourses);
  }

  render() {
    return (
      <View style={styles.main_container}>
        <Banner navigation={this.props.navigation} />
        <View style={styles.main_menu}>
            <FlatList
              style={styles.list}
              data={this.department}
              keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
              renderItem={({ item }) => (
                <Department
                  data={item}
                  allYears={this.year}
                  allSemesters={this.semester}
                  allUE={this.UE}
                  allCourses={this.courses}
                  choiceSemesters={this.choiceSemesters}
                  choiceUE={this.choiceUE}
                  choiceCourses={this.choiceCourses}
                />
              )}
              ListFooterComponent={<Nomenclature/>}
            >
            </FlatList>
        </View>

      </View>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  main_menu: {
    flex: 4
  },
  list:
  {
    flex: 1
  }
})
export default Syllabus
