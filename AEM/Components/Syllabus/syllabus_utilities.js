/*
 * Function which extracts entries of 'data' in 'dst' if their types match
 * with 'type_to_match'
 * -> 'data' is a list of entries having a 'Nature_EP' field
 * -> 'type_to_match' is a string corresponding to a 'Nature_EP' type,
 * e.g. "DIPLOME", "SEMESTRE_CHOIX" or "MODULE"
 * -> 'dst' is a table where matching entries are pushed 
 */
export function extractDataMatchingType(data, type_to_match, dst) {
    var data_length = Object.keys(data).length;
    for (var i = 0; i < data_length; i++) {
        if (data[i].Nature_EP == type_to_match) {
            dst.push(data[i]);
        }
    }
}

/*
 * Function which extracts entries of 'data' in 'dst' if their father match
 * with 'father_to_match'
 * -> 'data' is a list of entries having a 'Père' field
 * -> 'father_to_match' is a string corresponding to an indentification code of an entry,
 * typically the 'EP' field of a Syllabus entry  
 * -> 'dst' is a table where matching entries are pushed 
 */
export function extractDataMatchingFather(data, father_to_match, dst) {
    var data_length = Object.keys(data).length;
    for (var i = 0; i < data_length; i++) {
        if (data[i].Père == father_to_match) {
            dst.push(data[i]);
        }
    }
}

/*
 * Function which extracts entries of 'data' in 'dst' if their father match
 * with 'father_to_match'. 
 * This function is able to pick just one entry, even if it is duplicated in the Syllabus.
 * -> 'data' is a list of entries having a 'Père' field
 * -> 'father_to_match' is a string corresponding to an indentification code of an entry,
 * typically the 'EP' field of a Syllabus entry  
 * -> 'dst' is a table where matching entries are pushed 
 * To use only if data in Syllabus are duplicated, especially for courses. 
 */
export function extractSingleEntry(data, father_to_match, dst) {
    var data_length = Object.keys(data).length;
    for (var j = 0; j < data_length; j++) {
        // Case where we have severals identical key, so we have to filter them
        // to get a single course and not all its 'twins'
        var dst_length = Object.keys(dst).length;
        var isNotInCollection = true
        for (var i = 0; i < dst_length; i++) {
            if (dst[i].EP == data[j].EP) {
                isNotInCollection = false
            }
        }
        if ((data[j].Père == father_to_match) && (isNotInCollection)) {
            dst.push(data[j]);
        }
    }
}

/*
 * This function set the 'souldShow' variable in the state of the component 'component'.
 * It's called each time a user touches a component. 
 * Allows the components to hide (resp. to show) if they were showed (resp. hidden)
 * Due to the very long time of response of function extracting data, 
 * components may be not mounted before the state is changed. 'isMounted' variable is
 * supposed to fix this
*/
export function setShouldShow(component, shouldShow) {
    let isMounted = true;
    if(isMounted) {
        component.setState({ shouldShow: !shouldShow })
    }
    return () => {isMounted = false};
}

/*
 * This function returns true or false whether if 'data' contains data or not
*/
export function isThereData(data) {
    var length = Object.keys(data).length;
    if (length > 0) {
        return true
    }
    return false
}


