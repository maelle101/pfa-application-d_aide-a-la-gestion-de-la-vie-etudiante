import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

class SimpleItem extends React.Component {

    render() {
        return (
            <View style={this.props.styles.item}>
                <Text style={styles.title}>{this.props.title}</Text>
                {this.props.ects != "" ? (
                    <Text style={styles.ects}>{this.props.ects} crédits ECTS</Text>
                ) : null }
                {this.props.code != "" ? (
                    <Text style={styles.code}>Code {this.props.code}</Text>
                ) : null }
                {this.props.responsible != "" ? (
                    <Text>Encadré par {this.props.responsible}</Text>
                ) : null }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold'
    },
    ects: {
        fontStyle: 'italic'
    },
    code: {
        fontStyle: 'italic'
    }
})


export default SimpleItem
