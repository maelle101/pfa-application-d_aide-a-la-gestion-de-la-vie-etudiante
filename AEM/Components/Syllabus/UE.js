import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList } from 'react-native'

import Course from './Course'
import ChoiceCourse from './ChoiceCourse'
import SimpleItem from './SimpleItem'
import { setShouldShow, extractSingleEntry, isThereData } from './syllabus_utilities'

class UE extends React.Component {

  constructor(props) {
    super(props)
    this.state = { shouldShow: false }
    this.coursesOfUE = []
    this.choiceCoursesOfUE = []
    extractSingleEntry(this.props.allCourses, this.props.data.EP, this.coursesOfUE)
    extractSingleEntry(this.props.choiceCourses, this.props.data.EP, this.choiceCoursesOfUE)
    this.classicCase = isThereData(this.coursesOfUE)
    this.choiceCase = isThereData(this.choiceCoursesOfUE)
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code=""
            responsible={this.props.data.Responsables}
            styles={styles}
          />
        </TouchableOpacity>
        <View>
          {this.state.shouldShow ? (
            <View>
              {this.choiceCase ? (
                <FlatList
                  style={styles.list}
                  data={this.choiceCoursesOfUE}
                  keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
                  listKey={(index) => index.toString() + 'choice_course'}
                  renderItem={({ item }) => (
                    <ChoiceCourse
                      data={item}
                      allCourses={this.props.allCourses}
                    />
                  )}
                >
                </FlatList>
              ) : null}
              {this.classicCase ? (
                <FlatList
                  style={styles.list}
                  data={this.coursesOfUE}
                  keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
                  listKey={(index) => index.toString() + 'course'}
                  renderItem={({ item }) => (
                    <Course
                      data={item}
                    />
                  )}
                >
                </FlatList>
              ) : null}

            </View>
          ) : null}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#dadfac',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
})
export default UE
