import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList } from 'react-native'

import UE from './UE'
import SimpleItem from './SimpleItem'
import { setShouldShow, extractDataMatchingFather } from './syllabus_utilities'

class ChoiceUE extends React.Component {

  constructor(props) {
    super(props)
    this.state = { shouldShow: false }
    this.UEOfChoiceUE = []
    extractDataMatchingFather(this.props.allUE, this.props.data.EP, this.UEOfChoiceUE)
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code=""
            responsible={this.props.data.Responsables}
            styles={styles}
          />
        </TouchableOpacity>
        <View>
          {this.state.shouldShow ? (
            <FlatList
              style={styles.list}
              data={this.UEOfChoiceUE}
              keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
              renderItem={({ item }) => (
                <UE
                  data={item}
                  allCourses={this.props.allCourses}
                  choiceCourses={this.props.choiceCourses}
                />
              )}
            >
            </FlatList>
          ) : null}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#dadfac',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
})

export default ChoiceUE
