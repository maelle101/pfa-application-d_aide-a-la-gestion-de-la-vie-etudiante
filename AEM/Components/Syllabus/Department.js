import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList } from 'react-native'

// Import component responsible of the recursive-layout architecture
import Year from './Year'
// Import the component which displays a very common entry
import SimpleItem from './SimpleItem'
// Import utilities
import { setShouldShow, extractDataMatchingFather } from './syllabus_utilities'

class Department extends React.Component {

  constructor(props) {
    super(props)
    // State which allows to show & hide informations by touching it 
    // See 'setShouldShow' function (syllabus_utilities.js)
    this.state = { shouldShow: false }
    // Declare variable where matching data are stored
    this.yearsOfDepartment = []
    // Extract year entries according to the correct department 
    extractDataMatchingFather(this.props.allYears, this.props.data.EP, this.yearsOfDepartment)
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code=""
            responsible={this.props.data.Responsables}
            type={this.props.data.Nature_EP}
            styles={styles}
          />
        </TouchableOpacity>
        <View>
          {this.state.shouldShow ? (
            <FlatList
              style={styles.list}
              data={this.yearsOfDepartment}
              keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
              renderItem={({ item }) => (
                <Year
                  data={item}
                  allUE={this.props.allUE}
                  allCourses={this.props.allCourses}
                  allSemesters={this.props.allSemesters}
                  choiceSemesters={this.props.choiceSemesters}
                  choiceUE={this.props.choiceUE}
                  choiceCourses={this.props.choiceCourses}
                />
              )}
            >
            </FlatList>
          ) : null}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#99a744',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
})

export default Department
