import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList } from 'react-native'

import UE from './UE'
import ChoiceUE from './ChoiceUE'
import SimpleItem from './SimpleItem'
import { setShouldShow, extractSingleEntry, isThereData } from './syllabus_utilities'


class Semester extends React.Component {

  constructor(props) {
    super(props)
    this.state = { shouldShow: false }
    this.UEOfSemester = []
    this.choiceUEOfSemester = []
    extractSingleEntry(this.props.allUE, this.props.data.EP, this.UEOfSemester)
    extractSingleEntry(this.props.choiceUE, this.props.data.EP, this.choiceUEOfSemester)
    this.classicCase = isThereData(this.UEOfSemester)
    this.choiceCase = isThereData(this.choiceUEOfSemester)
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code=""
            responsible={this.props.data.Responsables}
            styles={styles}
          />
        </TouchableOpacity>
        <View>
          {this.state.shouldShow ? (
            <View>
              {this.choiceCase ? (
                <FlatList
                  style={styles.list}
                  data={this.choiceUEOfSemester}
                  keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
                  listKey={(index) => index.toString() + 'choice_UE'}
                  renderItem={({ item }) => (
                    <ChoiceUE
                      data={item}
                      allCourses={this.props.allCourses}
                      allUE={this.props.allUE}
                      choiceCourses={this.props.choiceCourses}
                    />
                  )}
                >
                </FlatList>
              ) : null}
              {this.classicCase ? (
                <FlatList
                  style={styles.list}
                  data={this.UEOfSemester}
                  keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
                  listKey={(index) => index.toString() + 'UE'}
                  renderItem={({ item }) => (
                    <UE
                      data={item}
                      allCourses={this.props.allCourses}
                      choiceCourses={this.props.choiceCourses}
                    />
                  )}
                >
                </FlatList>
              ) : null}
            </View>
          ) : null}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#cdd491',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
})

export default Semester
