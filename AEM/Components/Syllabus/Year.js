import React from 'react'
import { View, TouchableOpacity, StyleSheet, FlatList } from 'react-native'

import Semester from './Semester'
import ChoiceSemester from './ChoiceSemester'
import SimpleItem from './SimpleItem'
import { setShouldShow, extractDataMatchingFather, isThereData } from './syllabus_utilities'

class Year extends React.Component {

  constructor(props) {
    super(props)
    this.state = { shouldShow: false }
    this.semestersOfYear = []
    this.choiceSemestersOfYear = []
    extractDataMatchingFather(this.props.allSemesters, this.props.data.EP, this.semestersOfYear)
    extractDataMatchingFather(this.props.choiceSemesters, this.props.data.EP, this.choiceSemestersOfYear)
    // Because of the very special way entries are stored in the Syllabus,
    // some of 'Year' items are father of classic semesters and others
    // have 'ChoiceSemester' children. These variables are meant to display
    // correct data according their father 
    // Please refer to the report for more information
    this.classicCase = isThereData(this.semestersOfYear)
    this.choiceCase = isThereData(this.choiceSemestersOfYear)
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => setShouldShow(this, this.state.shouldShow)}
        >
          <SimpleItem
            title={this.props.data.Intitulé}
            ects={this.props.data.ECTS_CALCULE}
            code=""
            responsible={this.props.data.Responsables}
            styles={styles}
          />
        </TouchableOpacity>
        {this.state.shouldShow ? (
          <View>
            {
              // If we have 'SEMESTRE_CHOIX' entries, we display it in a
              // ChoiceSemester component
              this.choiceCase ? (
                <FlatList
                  style={styles.list}
                  data={this.choiceSemestersOfYear}
                  keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
                  listKey={(index) => index.toString() + 'choice_semester'}
                  renderItem={({ item }) => (
                    <ChoiceSemester
                      data={item}
                      allUE={this.props.allUE}
                      allCourses={this.props.allCourses}
                      allSemesters={this.props.allSemesters}
                      choiceUE={this.props.choiceUE}
                      choiceCourses={this.props.choiceCourses}
                    />
                  )}
                >
                </FlatList>
              ) : null}
            {
              // If we have classic 'SEMESTRE' entires, we display it in a
              // Semester component
              this.classicCase ? (
                <FlatList
                  style={styles.list}
                  data={this.semestersOfYear}
                  keyExtractor={(item, index) => (item.EP.toString() + index.toString())}
                  listKey={(index) => index.toString() + 'classic_semester'}
                  renderItem={({ item }) => (
                    <Semester
                      data={item}
                      allCourses={this.props.allCourses}
                      allUE={this.props.allUE}
                      choiceUE={this.props.choiceUE}
                      choiceCourses={this.props.choiceCourses}
                    />
                  )}
                >
                </FlatList>
              ) : null}
          </View>
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  list:
  {
    flex: 1
  },
  item: {
    backgroundColor: '#bfc976',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  }
})

export default Year
