import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text, FlatList } from 'react-native'

import { setShouldShow } from './syllabus_utilities'

class Nomenclature extends React.Component {

    constructor(props) {
        super(props)
        this.state = { shouldShow: false }
        this.nomenclature_data = require('../../Data/nomenclature.json')
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => setShouldShow(this, this.state.shouldShow)}
                activeOpacity={0.2}
            >
                <View style={styles.main_container}>
                    <View style={styles.header_container}>
                        <Text style={styles.title}>Modalité de Contrôle des Connaissances et des Compétences</Text>
                        <Text style={styles.subtitle}>Nomenclature</Text>
                    </View>
                    {
                        this.state.shouldShow ? (
                            <FlatList
                                data={this.nomenclature_data}
                                keyExtractor={(item, index) => (item.item.toString() + index.toString())}
                                renderItem={({ item }) => (
                                    <View style={styles.item_container}>
                                        <Text style={styles.item}>{item.item} : </Text>
                                        <Text style={styles.item_explanation}>{item.item_explanation}</Text>
                                    </View>
                                )}
                            />
                        ) : null
                    }
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: '#99a744',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16
    },
    header_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        flexWrap: 'wrap'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 15
    },
    subtitle: {
        fontStyle: 'italic'
    },
    item: {
        fontWeight: 'bold'
    },
    item_container: {
        backgroundColor: '#99a744',
        flexDirection: 'row',
        marginVertical: 8,
        marginHorizontal: 16,
    }
})
export default Nomenclature
