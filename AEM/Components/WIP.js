import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

class WIP extends React.Component {

  render() {
    return (
      <View style={styles.main_menu}> 
        <Text style={styles.default_text}>{this.props.description}</Text>
        <Image
          source={require('../assets/aem_logo.png')}
          style={styles.logo}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'flex-start'
  },
  main_menu: {
    flex: 4,
  },
  default_text: {
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20,
  },
  logo: {
    flex: 0.7,
    width: '100%',
    height: undefined,
    aspectRatio: 1,
    alignSelf: 'center',
    margin: 50,
  }
})

export default WIP
