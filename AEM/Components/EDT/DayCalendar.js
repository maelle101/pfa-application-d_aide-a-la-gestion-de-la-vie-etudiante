import React from 'react';
import { Alert, Dimensions, View } from 'react-native';
import * as FileSystem from 'expo-file-system';
import EventCalendar from '../../libEdt/EventCalendar';
import icsToJson from 'ics-to-json';
import moment from 'moment';
let { width } = Dimensions.get('window');
let {today} = new Date().setHours(8,0,0,0);
let id= 1;
/**
 * get events from file ics
 * @returns list of events
 */
async function loadEvents() {
    let result = null;
    let data = null;
    try {
        result = await FileSystem.readAsStringAsync(FileSystem.documentDirectory + 'cal.ics');
        data= icsToJson(result);
      for (let k in data) {
        let start=new Date(moment(data[k].startDate));
        let end=new Date(moment(data[k].endDate));
        let desc=data[k].description.split('\\n');
        let summ='';
        for (let i = 2; i < desc.length-1; i++) {
          summ=summ+desc[i];
          if(i!=desc.length-2)
          summ =summ+'\n';
        }
        data[k].id=id;
        id=id+1;
            data[k].color="#79BAEC";
            data[k].endDate=end;
            data[k].startDate=start;
            data[k].title=data[k].summary;
            data[k].summary=summ; 
            data[k].personal=false;
            if(desc[2] === 'CM')
            data[k].color="yellow";
            else if(desc[2] === 'CI')   
            data[k].color="#89C35C"; 
            else if(desc[2] === 'TD')   
            data[k].color="orange"; 
            else if(desc[2] === 'Projet')   
            data[k].color="pink"; 
            else if(desc[2] &&(desc[2].indexOf('Manifestation'))>-1)   
            data[k].color="#7D0552"; 
            else if(desc[2] &&((desc[2].indexOf('Examen'))>-1  ||(desc[2].indexOf('Soutenance'))>-1) ) 
              {data[k].color="#ED0000";
              if(desc[2] &&(desc[2].indexOf('seconde session'))>-1) 
              data[k].color="#FF8040";}
        }
    } catch(e) { 
        console.log(e);
    }
     return data; 
}



class DayCalendar extends React.Component {
  state = {
    events: [],
    personalEvents:[],
  };
    constructor(props) {
        super(props);
        loadEvents().then((data)=> {
          this.setState({personalEvents:this.props.personalEvents});
          this.setState({events:data})
       
        });
      }
    
      _eventTapped({title,summary, startDate, endDate,location,id,personal}) {
        if(personal==false){
          Alert.alert(
            `${title}`,
            
            `${summary}\n${location}\n${("0" + startDate.getHours()).slice(-2)}:${("0" + startDate.getMinutes()).slice(-2)} - ${("0" + endDate.getHours()).slice(-2)}:${("0" + endDate.getMinutes()).slice(-2)}`,
          );}
          else{
            Alert.alert(
              `${title}`,
              `${summary}\n${location}\n${("0" + startDate.getHours()).slice(-2)}:${("0" + startDate.getMinutes()).slice(-2)} - ${("0" + endDate.getHours()).slice(-2)}:${("0" + endDate.getMinutes()).slice(-2)}`,
              
              [
                {
                  text: "Supprimer",
                  onPress: () => {this.props.deleteEvent(id)},
                  style:"destructive"
                },
                {
                  text: "Modifier",
                  onPress: () => {this.props.callUpdateEvents(id)},
                  style: "cancel"
                },
                { text: "OK"}
              ]
            );
          }

      }
    
      render() {

        let {events,personalEvents} = this.state;
        personalEvents=this.props.personalEvents;
        let allEvents=events.concat(personalEvents);
        return (
          <View style={{ flex: 1, marginTop: 20 }}>
            <EventCalendar
              eventTapped={this._eventTapped.bind(this)}
              events={allEvents}
              width={width}
              initDate={today}
              format24h
              scrollToFirst
       
            />
          </View>
        );
      }
}

export default DayCalendar;