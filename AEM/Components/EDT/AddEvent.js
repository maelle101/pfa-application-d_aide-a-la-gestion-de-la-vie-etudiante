import React from 'react'

import { ScrollView, Alert, StyleSheet,View, Text,Pressable,TextInput} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Modal from 'react-native-modal';

/**
 * transform Date object to string dd/mm/yyyy
 * @param  currentDate 
 * @returns date
 */
function dateToString(currentDate) {
  let date="";
  if (typeof(currentDate) !== 'undefined'){
  const day=("0" + currentDate.getDate()).slice(-2);
  const month=("0" + (currentDate.getMonth() + 1)).slice(-2);
  const year=currentDate.getFullYear();
  date=day+"/"+month+"/"+year;}
  return date;
};
/**
 * transform Date object to string HH:mm
 * @param  currentDate 
 * @returns time
 */
var timeToString=function(currentDate){
  let time="";
  if (typeof(currentDate) !== 'undefined'){
  const minutes=(currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes();
  const hours=currentDate.getHours();
  time=hours+":"+minutes;}
  return time;
};

export default class AddEvent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        visible: this.props.visible,
        showDate:false,
        showBeginTime:false,
        showEndTime:false,
        title:'',
        date:'',
        beginTime:'',
        endTime:'',
        desc:'',
        location:'',

      };}
    /**
     * on Save button click
     */
    handleOk = () => {
        this.props.addEvent(false,
        {id:this.props.id,
        title: this.state.title,
        summary: this.state.desc,
        startDate:moment(this.state.date+this.state.beginTime,"DD/MM/YYYY HH:mm").toDate(),
        endDate: moment(this.state.date+this.state.endTime,"DD/MM/YYYY HH:mm").toDate(),
        location:this.state.location,
       color:'green'});
      };
    /**
      * on Dismiss button click
    */
    handleDismiss = () => {
      this.props.addEvent(false);
      };

   /**
   * on DatePicker Date Begin change
   * @param  event        
   * @param  selectedDate 
   */
    onChangeDate = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      const date=dateToString(currentDate);
      this.setState({showDate:false});
      this.setState({ date: date});};

    /**
     * on DatePicker Time Begin change
     * @param event 
     * @param selectedDate 
     */
    onChangeTimeBegin = (event, selectedDate) => {
      const currentDate = selectedDate || date; 
      const time=timeToString(currentDate);
      this.setState({showBeginTime:false});
      this.setState({ beginTime: time});};

    /**

    /**
    * on DatePicker Time End change
    * @param event 
    * @param selectedDate 
    */
    onChangeTimeEnd = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      const time=timeToString(currentDate);
      this.setState({showEndTime:false});
      this.setState({ endTime: time});};
 
    render(){
    const { visible,title,date,beginTime,endTime,desc,location} = this.state;
        return(
          
          <Modal propagateSwipe={true}
          animationType="slide"
          transparent={false}
          visible={visible}
          onRequestClose={() => {
            Alert.alert("Modal fermé");
            this.handleDismiss();
          }}
        ><ScrollView>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Ajouter un événement</Text>
              <Text style={styles.label}>Titre</Text>
              <TextInput style={styles.modalInput}
                      value={title}
                      onChangeText={text => {
                        this.setState({ title: text});}}
                    />
               <Text style={styles.label}>Description</Text>
              <TextInput style={styles.modalInput}
                      
                      value={desc}
                      onChangeText={text => {
                        this.setState({ desc: text});}}
                    />

            <Text style={styles.label}>Localisation</Text>
              <TextInput style={styles.modalInput}
                      
                     
                      value={location}
                      onChangeText={text => {
                        this.setState({ location: text});}}
                    />
                      <Text style={styles.label}>Date </Text>
              <TextInput style={styles.modalInput}
                     
                    value={date}
                    onFocus={()=>{this.setState({showDate:true})}}
                  
                    />
                       <Text style={styles.label}>Heure de début</Text>
              <TextInput style={styles.modalInput}
                    value={beginTime}
                    onFocus={()=>{this.setState({showBeginTime:true})}}
                   
                    />
                     
                    <Text style={styles.label}>Heure de fin</Text>
              <TextInput style={styles.modalInput}
                    value={endTime}
                    onFocus={()=>{this.setState({showEndTime:true})}}
                   
                    />
              <View style={[styles.buttons]} >
              <Pressable
                style={[styles.buttonDismiss]} 
                onPress={ this.handleDismiss}
              >
                <Text style={styles.textStyle}>Annuler</Text>
              </Pressable>
              <Pressable
               disabled={!title||!desc||!location||!date||!beginTime||!endTime}
               style={!title||!desc||!location||!date||!beginTime||!endTime? styles.buttonDisabled:styles.buttonClose} 
        
                onPress={ this.handleOk}
              >
                <Text style={styles.textStyle}>Ajouter</Text>
              </Pressable>
              
              </View>
            </View>
          </View>
        <View>
        {this.state.showDate &&(<DateTimePicker
        minimumDate={new Date()}
          value={new Date()}
          mode={'date'}
          display={Platform.OS === 'ios' ? 'spinner' : 'default'}
          is24Hour={true}
          onChange={this.onChangeDate}
        />)}
        {this.state.showBeginTime &&(<DateTimePicker
          
          value={new Date()}
          mode={'time'}
          display={Platform.OS === 'ios' ? 'spinner' : 'default'}
          is24Hour={true}
          onChange={this.onChangeTimeBegin}
        />)}
         
        {this.state.showEndTime &&(<DateTimePicker
          value={new Date()}
          mode={'time'}
          display={Platform.OS === 'ios' ? 'spinner' : 'default'}
          is24Hour={true}
          onChange={this.onChangeTimeEnd}
        />)}
        </View>
        
        </ScrollView>
        </Modal>
        )
    }
    
}

const styles = StyleSheet.create({
  centeredView: {
    flexDirection: "column",
    flex:1,
    justifyContent: "center",
  
    marginTop: 1,
  
  },
  buttons:{
    flexDirection: "row",

  },
  buttonClose: {
    flex:1,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    backgroundColor: "#2196F3",
  },
  buttonDismiss: {
    flex:1,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    backgroundColor: "#2196F3",
  },
  buttonDisabled: {
    flex:1,
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    backgroundColor: "grey",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalView: {
    flex:1,
    margin: 2,
    backgroundColor: "white",
    borderRadius: 10,
    padding: 15,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalInput:{
    width:'90%',
    height: 30,
    margin: 12,
    borderWidth: 1,
  },
  label:{
    textAlign:"left"
  }
});