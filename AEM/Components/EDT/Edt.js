import React from 'react';
import {StyleSheet, View,Text, Button, Dimensions} from 'react-native';
import WeekCalendar from './WeekCalendar';
import DayCalendar from './DayCalendar';
import AddEvent from './AddEvent';
import UpdateEvent from './UpdateEvent';
import Banner from '../Banner';

let id=1;
const {height, width} = Dimensions.get('window');
class Edt extends React.Component {
  /**
   * 
   * Constructor to initialise state
   */
  constructor(props) {
    super(props);
  let date=new Date();
  date.setHours(date.getHours() +2);
  this.state = {
    showWeek: true,
    showDay: false,
    addEvent:false,
    updateEvent:false,
    eventToUpdate:{},
    personalEvents: [{
      id:0,
      title:"Test",
      summary: "desc",
      startDate:new Date(),
      endDate: date,
      location:"loc",
      color:'green'
    }]
  };
   
    this.ShowComponent = this.ShowComponent.bind(this);
  
}
/**
 * to change week view and day view
 * @param name 
 */
ShowComponent(name) {
  switch (name) {
    case "showWeek":
      this.setState({ showWeek: true,showDay:false });
      break;
    case "showDay":
      this.setState({ showWeek: false,showDay:true });
      break;
    default:
      null;
  }
}
/**
 * to get the last generated id from weekCalendar
 * @param  i 
 */
getLastId=(i)=>{
id=i;
};

/**
 * change state to appear AddEvent Component
 */
callAddEvent(){
  this.setState({ addEvent: true });
}
/**
 * add event to list and disappear AddEvent Compnent
 * @param visible 
 * @param  ev 
 */
addEvent = (visible,ev) =>{
  this.setState({addEvent: visible});
  if(ev){
  this.setState({personalEvents: this.state.personalEvents.concat(ev)});
  id=ev.id+1;}
}
/**
 * delete event with event.id=id 
 * @param  id 
 */
deleteEvent = (id) =>{
  this.setState({personalEvents:this.state.personalEvents.filter(event=>event.id!=id)});
}
/**
 * appear UpdateEvent Component and pass EventToUpdate
 * @param  id 
 */
callUpdateEvent = (id) =>{
  this.setState({eventToUpdate:this.state.personalEvents.find(event=>event.id==id),updateEvent:true});
}
/**
 *  replace event and disappear UpdateEvent Component
 * @param  visible 
 * @param  ev 
 */
updateEvent=(visible,ev)=>{
  this.setState({updateEvent: visible});
  if(ev){
    this.setState({personalEvents:this.state.personalEvents.filter(event=>event.id!=ev.id).concat(ev)});
  }
}
  render() {
    const { showWeek, showDay, addEvent, updateEvent, eventToUpdate} = this.state;
    return (
      <>
        <Banner navigation={this.props.navigation}/>
        <View style={styles.main_container}>
        <View style={styles.edtContainer}>
          <View style={styles.add}>
        <Button
         title="Ajouter événement"
         onPress={() => this.callAddEvent()}
         color={'#228B22'}
        />
        </View>
        <View style={styles.nav}>
        <Button
         title="Sem."
         onPress={() => this.ShowComponent("showWeek")}
         color={showWeek?'#ff5c5c':'#2196F3'}
        />
        <Button
         title="Jours"
         onPress={() => this.ShowComponent("showDay")}
         color={showDay?'#ff5c5c':'#2196F3'}
        />
        </View>
        </View>
        {showWeek && < WeekCalendar uri={this.state.uri} personalEvents={this.state.personalEvents} deleteEvent={this.deleteEvent} callUpdateEvents={this.callUpdateEvent} getLastId={this.getLastId}/>}
        {showDay && < DayCalendar personalEvents={this.state.personalEvents} deleteEvent={this.deleteEvent} callUpdateEvents={this.callUpdateEvent}/>}
        {addEvent && < AddEvent visible={addEvent}  addEvent = {this.addEvent} id={id} />}
        {updateEvent && < UpdateEvent visible={updateEvent}  updateEvent = {this.updateEvent} event={eventToUpdate} />}
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 5
  },
  add:{
    flex:2,
    marginRight:width/100,
    marginLeft:width/100,
  },
  nav:{
    flex:1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent:'flex-end',
    marginRight:width/100,
    marginLeft:width/100,
  },
  edtContainer:{
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: 'space-between',
  },
});

export default Edt;



