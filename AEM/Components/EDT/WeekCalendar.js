
import React from 'react';
import {SafeAreaView, StyleSheet, StatusBar, Alert} from 'react-native';
import * as FileSystem from 'expo-file-system';
import WeekView from '../../libEdt/WeekView/WeekView';
import icsToJson from 'ics-to-json';
import moment from 'moment';
let id= 1;
/**
 * get events from file ics
 * @returns list of events
 */
async function loadEvents() {
    let result = null;
    let data = null;
    try {
        result = await FileSystem.readAsStringAsync(FileSystem.documentDirectory + 'cal.ics');
        data= icsToJson(result);
        
      for (let k in data) {
        
        let start=new Date(moment(data[k].startDate));
        let end=new Date(moment(data[k].endDate));
        let desc=data[k].description.split('\\n');
        let summ='';
        for (let i = 2; i < desc.length-1; i++) {
          summ=summ+desc[i];
          if(i!=desc.length-2)
          summ =summ+'\n';
        }
        data[k].id=id;
        id=id+1;
            data[k].color="#79BAEC";
            data[k].endDate=end;
            data[k].startDate=start;
            data[k].title=data[k].summary;
            data[k].summary=summ; 
            data[k].personal=false;
            if(desc[2] === 'CM')
            data[k].color="yellow";
            else if(desc[2] === 'CI')   
            data[k].color="#89C35C"; 
            else if(desc[2] === 'TD')   
            data[k].color="orange"; 
            else if(desc[2] === 'Projet')   
            data[k].color="pink"; 
            else if(desc[2] && (desc[2].indexOf('Manifestation'))>-1)   
            data[k].color="#7D0552"; 
            else if(desc[2] && ((desc[2].indexOf('Examen'))>-1  ||(desc[2].indexOf('Soutenance'))>-1)) 
              {data[k].color="#ED0000";
              if(desc[2] &&(desc[2].indexOf('seconde session'))>-1) 
              data[k].color="#FF8040";}
              
        }
    } catch(e) { 
        console.log(e);
    }
     return data; 
}


class WeekCalendar extends React.Component {
  state = {
    events: [],
    personalEvents:[],
    selectedDate: new Date(),
  };
  constructor(props){
    super(props);
    loadEvents().then((data)=> {
    this.setState({personalEvents:this.props.personalEvents});
    this.setState({events:data});
    this.props.getLastId(id);
  });
  }
  /**
   * show detail of event if personnal with options update and delete
   * @param {*} param0 
   */
onEventPress = ({title,summary, startDate, endDate,location,id,personal}) => {
  if(personal==false){
    Alert.alert(
      `${title}`,
      
      `${summary}\n${location}\n${("0" + startDate.getHours()).slice(-2)}:${("0" + startDate.getMinutes()).slice(-2)} - ${("0" + endDate.getHours()).slice(-2)}:${("0" + endDate.getMinutes()).slice(-2)}`,
    );}
    else{
      Alert.alert(
        `${title}`,
        `${summary}\n${location}\n${("0" + startDate.getHours()).slice(-2)}:${("0" + startDate.getMinutes()).slice(-2)} - ${("0" + endDate.getHours()).slice(-2)}:${("0" + endDate.getMinutes()).slice(-2)}`,
        
        [
          {
            text: "Supprimer",
            onPress: () => {this.props.deleteEvent(id)},
            style:"destructive"
          },
          {
            text: "Modifier",
            onPress: () => {this.props.callUpdateEvents(id)},
            style: "cancel"
          },
          { text: "OK"}
        ]
      );
    }
  };

 
  render() {
    let {events,personalEvents, selectedDate} = this.state;
    personalEvents=this.props.personalEvents;
    let allEvents=events.concat(personalEvents);
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.container}>
          <WeekView
            ref={r => {
              this.componentRef = r;
            }}
            events={allEvents}
            selectedDate={selectedDate}
            numberOfDays={7}
            onEventPress={this.onEventPress}
            onGridClick={this.onGridClick}
            headerStyle={styles.header}
            headerTextStyle={styles.headerText}
            hourTextStyle={styles.hourText}
            eventContainerStyle={styles.eventContainer}
            formatDateHeader="MMM D"
            hoursInDisplay={12}
            startHour={8}
          />
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    paddingTop: 22,
  },
  header: {
    backgroundColor: '#4286f4',
    borderColor: '#fff',
  },
  headerText: {
    color: 'white',
  },
  hourText: {
    color: 'black',
  },
  eventContainer: {
    borderWidth: 1,
    borderColor: 'black',
  },
  edt:{
    flex:1,
    padding:10,
    textAlign: 'center', 
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 10,
  },
  edtContainer:{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
  
  },
});

export default WeekCalendar;



