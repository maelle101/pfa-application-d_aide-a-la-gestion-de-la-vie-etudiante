import React from 'react'
import { View, StyleSheet} from 'react-native'
import Banner from './Banner'
import WIP from './WIP'

class Home extends React.Component {
    state = {
        events: [],
        id: 0,
    };

    constructor(props) {
        super(props);


    }

  render() {
    return (
      <View style={styles.main_container}>
        <Banner navigation= {this.props.navigation} />
        <WIP
          description="Ici seront bientôt disponibles des informations sur l'utilisateur."
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignContent: 'flex-start'
  }
})

export default Home
