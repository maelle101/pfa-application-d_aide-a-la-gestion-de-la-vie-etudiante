import {View, StyleSheet} from 'react-native'
import React from 'react'
import ChoiceRoomTextArea from "./ChoiceRoomTextArea";
import Banner from "../Banner";


class Map extends React.Component {
    render() {
        return (
            <View style={styles.main_container}>
                <Banner navigation={this.props.navigation}/>
                <View style={styles.main_interface}>
                    <ChoiceRoomTextArea/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        flexDirection: 'column'
    },
    main_interface: {
        flex: 4
    },
    default_text: {
        fontWeight: 'bold',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 20
    }
})

export default Map