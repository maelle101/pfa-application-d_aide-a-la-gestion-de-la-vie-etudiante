import React from 'react'
import {StyleSheet, View, TextInput, Button, ActivityIndicator} from "react-native"
import DisplayRoom from "./DisplayRoom";

class ChoiceRoomTextArea extends React.Component {
    constructor(props) {
        super(props)
        this.searchedText = ""
        this.state = {room: "", isLoading: false}
    }

    _loadRoom() {
        if (this.searchedText.length > 0) {
            this.setState({isLoading: true})
            this.setState({room: this.searchedText})
            this.setState({isLoading: false})
        }
    }

    _searchTextInputChanged(text) {
        this.searchedText = text
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large'/>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={styles.main_container}>
                <TextInput style={styles.textinput} placeholder='Nom de la Salle'
                           onChangeText={(text) => this._searchTextInputChanged(text)}
                           onSubmitEditing={() => this._loadRoom()}
                />
                <Button title='Rechercher' onPress={() => {
                    this._loadRoom()
                }}/>
                <DisplayRoom styles={{height: 1000}} room={this.state.room}/>
                {this._displayLoading()}
            </View>
        )
    }
}

const
    styles = StyleSheet.create({
        main_container: {
            flex: 1,
            marginTop: 20
        },
        textinput: {
            marginLeft: 5,
            marginRight: 5,
            height: 50,
            borderColor: '#000000',
            borderWidth: 1,
            paddingLeft: 5
        },
        loading_container: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 100,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center'
        }
    })

export default ChoiceRoomTextArea