import React from 'react'
import { View, StyleSheet, Text, Linking } from 'react-native'
import Banner from './Banner'
import WIP from './WIP'

class Mailbox extends React.Component {

  render() {
    return (
      <View style={styles.main_container}>
        <Banner navigation= {this.props.navigation} />
        <WIP
          description="Ici sera bientôt disponible la boîte mail."
        />
        <View style={styles.text_container}>
          <Text 
            style={styles.text}
            onPress={() => Linking.openURL('https://boring-elion-65bca1.netlify.app/')}
          >
          Lien vers l'aperçu web
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text_container: {
    flex: 2,
    padding: 10,
    alignItems: 'center'
  },
  text: {
    fontWeight: 'bold',
    color: 'blue',
    fontSize: 20,
  },
  url: {
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default Mailbox
