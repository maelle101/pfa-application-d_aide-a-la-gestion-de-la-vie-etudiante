import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native'

class Banner extends React.Component {

    render () {
        return (
                <View style={styles.banner}>
                    <TouchableOpacity 
                    style={styles.button}
                    onPress={ () => this.props.navigation.toggleDrawer()}>
                      <Image
                        style={styles.tiny_logo}
                        source={require('../assets/Hamburger_icon.png')}
                      />
                    </TouchableOpacity>
                    <View style={styles.informations}>
                        <Text style={styles.default_text} >Bonjour Alice !</Text>
                    </View>
                    <Text>{this.props.jdjdjd}</Text>
                </View>          
        )

    }
}

const styles = StyleSheet.create({
    banner: {
      flex: 0.5,
      paddingTop: 10,
      flexDirection: 'row',
      alignContent: 'center',
      alignItems: 'center',
    },
    button: {
      flex: 1,
      margin: 10
    },
    informations: {
      flex: 3
    },
    default_text: {
      flex: 1,
      fontWeight: 'bold',
      textAlign: 'center',
      textAlignVertical: 'center',
      fontSize: 20
    },
    tiny_logo: {
      width:50,
      height:50,
      alignSelf: 'center'
    }
  })
  
  export default Banner