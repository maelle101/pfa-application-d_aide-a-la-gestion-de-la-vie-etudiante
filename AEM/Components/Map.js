import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import Banner from './Banner'
import WIP from './WIP'

class Map extends React.Component {

  render() {
    return (
      <View style={styles.main_container}>
        <Banner navigation= {this.props.navigation} />
        <WIP
          description="Ici sera bientôt disponible la carte."
        />
        <Text style={styles.text}>Aperçu web : http://aem.eirb.fr/map</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    flex: 2,
    fontWeight: 'bold'
  }
})

export default Map
