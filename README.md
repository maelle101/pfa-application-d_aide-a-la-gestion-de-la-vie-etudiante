# PFA - Application d'aide à la gestion de la vie étudiante

_Written in May 2021 for the first release of the project_

## Context 

This project took place between November 2020 and May 2021. It is part of the ENSEIRB-MATMECA's Year-Long Project (Projet au Fil de l'Année - PFA). 

Its contributors are 

- Marc Duclusaud
- Maelle Andricque
- Mehdi Ben Salah
- Vincent Carton
- Maxime Despres
- Aymeric Ferron
- Omayma Zouch

Link towards our school : https://enseirb-matmeca.bordeaux-inp.fr/fr

## Overview

This project aims at gathering in the same place several services related to the student life of ENSEIRB-MATMECA.

It includes :

- a schedule module 
- a Syllabus module
- a map module
- a draft of a mailbox module

This application is intended to be completed with 
- an instant messaging module
- a voluntary news module

## Technologies

React Native has been used to developp this project. 

## Architecture

The root of the repository contains three folders :

- **AEM** is the mobile application. 
- **Build** is the folder containing our application under an _apk_ format.
- **Converter** is a JS app used to convert .xls files into JSON file. It is used for the Syllabus module.
- **Doc** contains several reports wirtten by the dev team. This documentation can help a better understanding of our work. Available in french only.
    - _Cahier\_des\_charges_ : contains specifications
    - _Rapport\_final_ : overview of our work at the end of the project
    - _Manuel\_d\_utilisation_ : user's guide
    - _Manuel\_de\_maintenance_ : maintenance guide
- **MailboxDraft** contains a draft of a Mailbox written in a web language.
- **Ressources** contains the map of ENSEIRB that can be edited


## Run the application

If you want to test the app on your own server, we recommand you to use Expo (https://expo.io/).

In your terminal, in **AEM/** folder, run the following instructions :

1. npm install (_installs dependencies_)
2. npm start (_starts server_)

Then, you can use an emulator or Expo Go (https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_US&gl=US) to test the project

## Build the application

Please, consider this following tutorial :  https://docs.expo.io/distribution/building-standalone-apps/

