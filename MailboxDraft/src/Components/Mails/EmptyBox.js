/* IMPORTS */

// Library imports
import React from 'react';


// Local file imports
import './mail.css';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';

// Default empty email box
class EmptyBox extends React.Component {

  render(){
    return (
      <p className="center">Aucun message</p>
    )
  }
}

export default EmptyBox;